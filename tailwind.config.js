module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'matisse': '#2179ac',
      },
    },
  },
  plugins: [],
}
