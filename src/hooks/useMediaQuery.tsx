import React from "react";

const useMediaQuery = (query: string, whenTrue: any, whenFalse: any):any => {
  
    const mediaQuery = window.matchMedia(query);
    const [match, setMatch] = React.useState(!!mediaQuery.matches);
  
    React.useEffect(() => {
      const handler = () => setMatch(!!mediaQuery.matches);
      mediaQuery.onchange = handler;
      return () => { mediaQuery.onchange = null };
    }, [mediaQuery]);
  
    return match ? whenTrue : whenFalse;
};

export default useMediaQuery