export const screenIs = (screen = '') => {
    // "Theme" is an alias to where you keep your tailwind.config.js - most likely your project root
    const screens = {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px',
    }
  
    // create a keyed object of screens that match
    const matches = Object.entries(screens).reduce((results, [name, size]) => {
      const mediaQuery = typeof size === 'string' ? `(min-width: ${size})` : `(max-width: ${size.max})`;
  
      results[name] = mediaQuery;
  
      return results;
    }, {});
  
    // show all matches when there is no screen choice
    if (screen === '') {
      return matches;
    }
  
    // invalid screen choice
    if (!screens[screen]) {
      console.error(`No match for "${screen}"`);
  
      return false;
    }
  
    return matches[screen];
  };