import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
  createBrowserRouter,
  RouterProvider
} from "react-router-dom";
import Documents from './components/Documents';
import Layout from './views/FrontPage/Layout'
import Default from './views/FrontPage/Default'
import Player from './views/Player'


const router = createBrowserRouter([{
  path:"/",
  element: (<Layout/>),
  children: 
  [{
    index: true,
    element:(<Default/>)
  },{
    path:'docs',
    element:(<Documents/>)
  }]
},{
  path:"/player",
  element: (<Player/>)
}]);


ReactDOM.render(
  <React.StrictMode>
        <RouterProvider router={router} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
