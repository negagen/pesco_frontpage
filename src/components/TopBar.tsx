

//academic-cap
//book-open
//cube
//desktop-computer
//puzzle 
//chart-square-bar
import {Link} from 'react-router-dom'


export default function TopBar() {
 
    
    return (
        <div className="relative bg-white">
            <div className="max-w-7xl mx-auto px-4 sm:px-6">
                <div className="flex justify-between items-center py-6 md:justify-start md:space-x-10">
                    <div className="flex justify-start lg:w-0 lg:flex-1">
                        <Link className="flex px-4" to="/">
                            <span className="sr-only">Pesco</span>
                            <img
                                className="h-12 w-auto h-20  "
                                src="/logo192.png"
                                alt=""
                            />
                             <div className="text-4xl tracking-wider mx-4 font-bold my-auto align-middle text-[#2179ac]">
                                PESCO
                            </div>
                        </Link>
                       
                    </div>
                    
                </div>
            </div>

            
        </div>
    )
}