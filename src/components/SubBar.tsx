import { useState } from 'react'
import { Popover } from '@headlessui/react'
import {
    AcademicCapIcon,
    BookOpenIcon,
    PuzzleIcon,
    ChartSquareBarIcon
} from '@heroicons/react/outline'
import {
    Link
} from 'react-router-dom'

interface NavItemProps {
    Icon: React.FC<React.ComponentProps<'svg'>>,
    text: String,
    url: string,
    local: boolean
}


function NavItem(props: NavItemProps) {
    const [hidden, setHidden] = useState(false);
    let classNames = 'my-auto buttonText  text-sm font-semibold p-1';

    /*if (hidden) {
        classNames += ' hidden';
    }

    const showText = () => {
        setHidden(false);
    }

    const dontShow = () => {
        setHidden(true);
    }*/

    return props.local ? <Link/*onMouseOver={showText} onMouseLeave={dontShow}*/ to={props.url} className='hover:bg-white py-5 md:py-3 px-8 md:px-4 flex text-white hover:text-matisse w-full md:w-auto'>
        <props.Icon className='h-7 mr-3' />
        <a className={classNames}>{props.text}</a>
    </Link> : <a href={props.url}/*onMouseOver={showText} onMouseLeave={dontShow}*/ className='hover:bg-white py-5 md:py-3 px-8 md:px-4 flex text-white hover:text-matisse w-full md:w-auto'>
        <props.Icon className='h-7 mr-3' />
        <div className={classNames}>{props.text}</div>
    </a>
}

export default function SubBar() {
    const menuItems = [
        {
            icon: ChartSquareBarIcon,
            text: 'Modelado y DS',
            url: 'http://simon.uis.edu.co',
            local: false,
        },
        {
            icon: AcademicCapIcon,
            text: 'Moodle',
            url: '/moodle',
            local: false,
        },
        {
            icon: PuzzleIcon,
            text: 'Estanque virtual',
            url: '/player',
            local: true,
        },
        {
            icon: BookOpenIcon,
            text: 'Documentación',
            url: '/docs',
            local: true,
        },
    ]
    return <Popover className="bg-sky-600">
        <div className="max-w-7xl mx-auto md:px-4 md:px-6">
            <div className="md:flex justify-between items-center md:justify-start md:space-x-10">
                <div className="md:flex justify-start lg:w-0 lg:flex-1">

                    {
                        menuItems.map((item) => {
                            return <NavItem Icon={item.icon} url={item.url} local={item.local} text={item.text} />
                        })
                    }
                </div>

            </div>
        </div>
    </Popover>
}  