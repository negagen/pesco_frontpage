import React, { useCallback } from 'react'
import { Transition } from '@headlessui/react'


let heroInfo = {
    "images": [
        {
            'alt': 'Hero1',
            'src': '/img/hero5.png',
        },
        {
            'alt': 'Hero2',
            'src': '/img/hero6.png'
        },
        {
            'alt': 'Hero3',
            'src': '/img/hero7.png'
        }
    ]
}

export default function Hero() {    
    const divRef = React.createRef<HTMLDivElement>();
    const [images, ] = React.useState<HTMLImageElement[]>(
        heroInfo.images.map<HTMLImageElement>((image,index)=>{
            let resultImage = new Image();
            resultImage.src = image.src;
            resultImage.alt = image.alt;
            return resultImage;
        })
    )

    const [index, setIndex] = React.useState(0);
    const [bufferIndex, setBuffer] = React.useState(0);
    const [showTransition, setShow] = React.useState(false);

    const changeImageOnHero = useCallback((index:number)=>{
        if(index<0){
            changeImageOnHero(images.length+index)
        }

        if(index>=images.length){
            changeImageOnHero(index-images.length)
        }
        
        if(0<=index && index<images.length){
            setBuffer(index)
            setShow(true);
        }
    }, [images.length])

    const prevImage = (event:React.MouseEvent) => {
        changeImageOnHero(index-1)
    }

    const nextImage = (event:React.MouseEvent) => {
        changeImageOnHero(index+1);
    }

    const afterEnterTransition = ()=>{
        setIndex(bufferIndex)
        setShow(false);
    }

    React.useEffect(() => {
        
        const interval = setInterval(() => {
            changeImageOnHero(index+1)
        }, 5000);

        return () => clearInterval(interval);
    }, [changeImageOnHero, index, divRef]);



    return (
        <div ref={divRef} className="relative mx-auto hidden md:block">
            <button onClick={prevImage} className='transition-opacity absolute left-0 px-10 text-white/0 hover:text-white/60 inset-y-0 text-[50px] duration-300'>{"<"}</button>

            <div className='flex relative bg-sky-600 -z-10 xl:h-[800px] md:h-[600px]'>
                <img className='absolute top-0 left-0 object-left-top xl:h-[800px] md:h-[600px] w-full x object-cover bg-sky-600' src={images[index].src} alt={images[index].alt}/>
                <Transition 
                    enter="transition-opacity duration-200"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leaveFrom="visible"
                    leaveTo="invisible"
                    afterEnter={afterEnterTransition}
                    show={showTransition}
                >
                    <img className='absolute top-0 left-0 object-left-top x w-full xl:h-[800px] md:h-[600px] object-cover bg-sky-600' src={images[bufferIndex].src} alt={images[bufferIndex].alt}/>
                </Transition>
            </div>

            <button onClick={nextImage} className='transition-opacity absolute right-0 px-10 text-white/0 hover:text-white/60 inset-y-0 text-[50px] duration-300'>{">"}</button>
        </div>
    )
}
