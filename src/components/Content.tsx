import React from 'react'
import { screenIs } from '../util/screenIs'
import useMediaQuery from '../hooks/useMediaQuery'

function ContentHolder({ className, ...restProps }: React.ComponentPropsWithRef<'div'>) {

    className += " justify-between flex flex-col md:flex-row flex-initial px-4 sm:px-6 max-w-7xl mx-auto"
    return <div className={className} {...restProps}>
    </div>
}

export default function Content() {
    const screenIsMd = useMediaQuery(screenIs("md"), true, false)
    const styleOrder = !screenIsMd ? {order:-1} : {}

    return (
        <div className='py-6 md:py-28'>
            <ContentHolder className=''>
                <div className='my-auto md:ml-0 md:mr-3 lg:mr-8 md:w-full'>
                    <div>
                        <h2 className='pb-5 text-matisse font-bold text-2xl tracking-wide'>
                            ¡Nuevo pesco!
                        </h2>
                        <p className='text-lg'>
                            Juego serio PESCO versión 4.0 funciona como un simulador de practica, para la producción de piscicultura en un estanque de geomembrana, el cual ayuda a que se complementen las experiencias que se obtienen en la teoría y se mejore con los datos obtenidos a través de la practica real del estanque de campo.
                        </p>
                    </div>
                </div>
                <div style={styleOrder} className='p-4 sm:p-8 md:p-0 mb-4 w-full md:ml-3 lg:ml-8'>
                    <img src="/img/card7.png" className='border-[20px] border-white drop-shadow drop-shadow-[0_0_0.1rem_rgba(0,0,0,0.50)] bg-black w-full aspect-square' alt="Aqui va una imagen" />
                </div>
                
            </ContentHolder>
            <ContentHolder className='pt-0 md:pt-20'>
            
                <div className='p-4 sm:p-8 md:p-0  w-full md:mr-3 lg:mr-8'>
                    <img src="/img/card1.jpg" className='border-[20px] border-white drop-shadow drop-shadow-[0_0_0.1rem_rgba(0,0,0,0.50)] bg-black w-full aspect-square' alt="Aqui va una imagen" />
                </div>
                <div className='my-auto md:mr-0 md:ml-3 lg:ml-8 md:w-full'>
                    <div>
                        <h2 className='pb-5 text-matisse font-bold text-2xl tracking-wide'>
                            Producción con estanque de geomembrana en forma cilindrica
                        </h2>
                        <p className='text-lg'>
                            Piscicultura de tilapia roja llevada a cabo por el grupo de investigación SIMON en asociación con el IPRED, como parte del proyecto de investigación LAPIS, Se utiliza para intensificar la producción en los sistemas de recirculación, acuaponía y el biofloc. La ventaja de esta alternativa es que mientras en los sistemas tradicionales se siembran entre 1 y 5 peces por metro cuadrado, si se cosechan de
                            10 una libra, se obtendrían de 2 a 3 kilos por metro cúbico; mientras que, en tanques de geomembrana, con un diseño técnico adecuado y unos equipos de óptima calidad pueden lograrse hasta 100 kilos por metro cubico. Se desarrolla en condiciones cuya temperatura promedio ambiente es entre 19° a 27°C, ubicada en una finca de Piedecuesta - Santander.
                        </p>
                    </div>
                </div>
                
            </ContentHolder>
            <ContentHolder className='pt-0 md:pt-20'>
                <div className='my-auto md:ml-0 md:mr-3 lg:mr-8 md:w-full'>
                    <div>
                        <h2 className='pb-5 text-matisse font-bold text-2xl tracking-wide'>
                            Producción con estanque de geomembrana: alimentación
                        </h2>
                        <p className='text-lg'>
                            La alimentación es un factor importante en la cría de peces, como lo es en toda la producción animal. Además de la nutrición natural, los peces deben recibir comida balanceada, la cual debe estar acorde a las necesidades nutricionales de cada especie y principalmente a su peso y temperatura del agua. Por esa razón, el piscicultor debe aprender las técnicas de medición y pesaje apropiadas a cada especie y etapa del crecimiento, técnicas generalmente de operación manual y de impactos negativos para la productividad al ser aplicados.
                        </p>
                    </div>
                </div>
                <div style={styleOrder} className='p-4 sm:p-8 md:p-0  w-full md:ml-3 lg:ml-8'>
                    <img src="/img/card3.jpg" className='border-[20px] border-white drop-shadow drop-shadow-[0_0_0.1rem_rgba(0,0,0,0.50)] bg-black w-full aspect-square' alt="Aqui va una imagen" />
                </div>
            </ContentHolder>
            <ContentHolder className='pt-0 md:pt-20'>
                <div className='p-4 sm:p-8 md:p-0 w-full md:mr-3 lg:mr-8'>
                    <img src="/img/card4.jpg" className='border-[20px] border-white drop-shadow drop-shadow-[0_0_0.1rem_rgba(0,0,0,0.50)] bg-black w-full aspect-square' alt="Aqui va una imagen" />
                </div>
                <div className='my-auto md:mr-0 md:ml-3 lg:ml-8 md:w-full'>
                    <div>
                        <h2 className='pb-5 text-matisse font-bold text-2xl tracking-wide'>
                            Producción con estanque de geomembrana: sistema de aguas
                        </h2>
                        <p className='text-lg'>
                            Es importante que el agua no contenga contaminantes organofosforados, fertilizantes o agroquímicos, así como evitar agua contaminada con aguas negras. Se requiere un mínimo de 3.5 ppm (partes x millón) de oxígeno disuelto y la mayoría de las aguas disponibles lo tienen.
                        </p>
                    </div>
                </div>
            </ContentHolder>
            <ContentHolder className='pt-0 md:pt-20'>
                <div className='my-auto md:ml-0 md:mr-3 lg:mr-8 md:w-full'>
                    <div>
                        <h2 className='pb-5 text-matisse font-bold text-2xl tracking-wide'>
                            Producción con estanque de geomembrana: sistema de aireación
                        </h2>
                        <p className='text-lg'>
                            En los últimos años se han implementado sistemas de recirculación de agua, y de aireación artificial. La oxigenación del agua, en especial en épocas de verano, y en horas de la noche por acción de las algas acuáticas, es uno de los factores más críticos para el desarrollo del cultivo, ya que aspectos como la oxidación de las proteínas, hidratos de carbono y grasas, requeridas para su desarrollo corporal, dependen del grado de aireación. Eventos como descomposición de residuos orgánicos no disueltos, heces, nubosidad y densidad de siembra, afectan el grado de oxígeno disuelto en el agua, y a pesar de ser la Tilapia más tolerante que otras especies, los bajos niveles de aireación (1,0 mg/l), les genera estrés que se traduce en presentación de patologías e incrementos de la mortalidad.
                        </p>
                    </div>
                </div>
                <div style={styleOrder} className='p-4 sm:p-8 md:p-0 w-full md:ml-3 lg:ml-8'>
                    <img src="/img/card5.jpg" className='border-[20px] border-white drop-shadow drop-shadow-[0_0_0.1rem_rgba(0,0,0,0.50)] bg-black w-full aspect-square' alt="Aqui va una imagen" />
                </div>
            </ContentHolder>
            <ContentHolder className='pt-0 md:pt-20'>
                <div className='p-4 sm:p-8 md:p-0 w-full  md:mr-3 lg:mr-8'>
                    <img src="/img/card6.jpg" className='border-[20px] border-white drop-shadow drop-shadow-[0_0_0.1rem_rgba(0,0,0,0.50)] bg-black w-full aspect-square' alt="Aqui va una imagen" />
                </div>
                <div className='my-auto md:mr-0 md:ml-3 lg:ml-8 md:w-full'>
                    <div>
                        <h2 className='pb-5 text-matisse font-bold text-2xl tracking-wide'>
                            Producción con estanque de geomembrana: sistema de monitoreo

                        </h2>
                        <p className='text-lg'>

                            Se monitorean tres elementos esenciales en la piscicultura nivel de Oxigeno, nivel de pH y temperatura, las cuales en el sistema de automatización se envían por protocolo LoRa al dispositivo de adquisición ubicado en la cabaña de monitoreo cerca de los estanques y se suben a través de una conexión a internet por html al servidor del grupo.

                        </p>
                    </div>
                </div>
            </ContentHolder>
        </div>
    )
}
