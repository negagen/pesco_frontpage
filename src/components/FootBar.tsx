import React from 'react'

export default function FootBar() {
    return (
        <div className='bg-gray-200'>
        <div className='max-w-7xl mx-auto px-4 sm:px-6'>
            <div className='flex py-14 rounded-md text-base font-medium'>
                UIS © 2022 | Grupo de investigación en modelos y simulacion - SIMON | Todos los derechos reservados.
            </div>
        </div>
        </div>
    )
}
