import { Unity, useUnityContext } from "react-unity-webgl";


export default function Player() {
    const { unityProvider } = useUnityContext({
        loaderUrl: process.env.PUBLIC_URL + "/build/player.loader.js",
        dataUrl:  process.env.PUBLIC_URL + "/build/player.data.gz",
        frameworkUrl: process.env.PUBLIC_URL + "/build/player.framework.js.gz",
        codeUrl: process.env.PUBLIC_URL + "/build/player.wasm.gz",
      });
    
    return <Unity style={{
        width: "100vw",
        height: "100vh",
    }} unityProvider={unityProvider} />;
}