import '../FrontPage.css';
import TopBar from '../../../components/TopBar';
import FootBar from '../../../components/FootBar';
import SubBar from '../../../components/SubBar';
import { Outlet } from "react-router-dom";

function App() {
  return (
    <div>
      <TopBar/>
      <SubBar/>
      <Outlet/>
      <FootBar/>
    </div>
  );
}

export default App;
