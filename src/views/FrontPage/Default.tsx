import './FrontPage.css';
import Hero from '../../components/Hero';
import Content from '../../components/Content';
import { screenIs } from '../../util/screenIs';
import useMediaQuery from '../../hooks/useMediaQuery';

function App() {
  const screenIsMd = useMediaQuery(screenIs("md"), true, false)

  return (
    <>
      {screenIsMd ? <Hero/> : <></>}
      <Content/>
    </>
  );
}

export default App;
